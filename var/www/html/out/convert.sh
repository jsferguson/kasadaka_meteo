#!/bin/bash


for f in /var/www/html/out/*.wav
do
  path=$f
  file=$(basename $path)
  sox "$f" -r 8000 -c 1 -s /var/www/html/$1/$file;
  rm $f
  #echo $f
done
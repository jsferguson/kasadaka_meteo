<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:01
 */
include('get_db.php');

/*
 * PDO is used to communicate with the SQLite database. This is the preferred method, as PDO makes it easy to switch
 * to a different database system without much changes to the code.
 */
function insertData() {

    $file_db = getDB();
    $insert = "INSERT INTO CountryLanguages (languageID, countryID) VALUES (:languageID, :countryID)";
    $stmt = $file_db->prepare($insert);


    $stmt->bindParam(':languageID', $_POST['languageID']);
    $stmt->bindParam(':countryID', $_POST['countryID']);

    try {
        $stmt->execute();
        $file_db = null;
    }

        /*
         * if the INSERT failed, modify the return confirmation, so the JavaScript AJAX call is informed of the failure.
         */
    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }
}

/*
 * This PHP-file receives JSON-data from the application and stores it in the SQLite database.
 */

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data


/*
 * for each expected data in the POST array, check if there actually is some data.
 * If a value is empty, add an error to the validation error array
 */

if (empty($_POST['languageID']))
    $errors['languageID'] = 'LanguageID is required.';

if (empty($_POST['countryID']))
    $errors['countryID'] = 'CountryID is required.';
/*
 * If there are any errors in the validation errors array, set the success boolean to FALSE
 */
if ( ! empty($errors)) {

    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
} else {

    /*
     * there are no errors, so set the success boolean to TRUE
     */
    $data['success'] = true;
    $data['message'] = 'Succeeded';
}

insertData();
echo json_encode($data);
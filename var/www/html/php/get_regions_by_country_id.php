<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 10:10
 */
include('get_db.php');

try {


    $file_db = getDB();
    $stmt = $file_db->query("select regionID, name from Regions where countryID = :countryID order by name;");
    $stmt->bindParam(':countryID', $_POST['countryID']);

    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
}
catch (PDOException $e) {

    echo $e;
}
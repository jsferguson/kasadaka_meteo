<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 10:10
 */
include('get_db.php');

try {


    $file_db = getDB();
    $stmt = $file_db->query("select placeID, name, lattitude, longtitude from Places where regionID=:regionID;");
    $stmt->bindParam(':regionID', $_POST['regionID']);

    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
}
catch (PDOException $e) {

    echo $e;
}
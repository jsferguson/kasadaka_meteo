<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:19
 */
include('get_db.php');
$country_id = $_POST['countryID'];
$language_id = $_POST['languageID'];

$places = get_place_names($country_id, $language_id);


delete_place_files($places);
deleteFromDB($language_id, $country_id);

function delete_place_files($places){

    foreach ($places as $place_name){

        $filename = '/var/www/html/'.$place_name['language_code'].'/'.$place_name['place_name'].'.wav';
        if(file_exists($filename)){

            unlink($filename);
        }
        $filename = '/var/www/html/'.$place_name['language_code'].'/'.$place_name['region_name'].'.wav';
        if(file_exists($filename)){

            unlink($filename);
        }
        $filename = '/var/www/html/'.$place_name['language_code'].'/'.$place_name['country_name'].'.wav';
        if(file_exists($filename)){

            unlink($filename);
        }
    }
}

function deleteFromDB($language_id, $country_id) {

    $file_db = getDB();
    $file_db->exec('PRAGMA foreign_keys = ON;');

    $delete = "DELETE FROM CountryLanguages WHERE languageID = :languageID AND countryID = :countryID";
    $stmt = $file_db->prepare($delete);


    $stmt->bindParam(':languageID', $language_id);
    $stmt->bindParam(':countryID', $country_id);



    try {
        $stmt->execute();
        $file_db = null;
    }


    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }
}

function get_place_names($country_id, $language_id){


    $file_db = getDB();
    $sql = "select Countries.name as country_name, Languages.code as language_code, Countries.code as country_code, Regions.name as region_name, Places.name as place_name from Countries join CountryLanguages ON Countries.countryID = CountryLanguages.countryID join Languages ON Languages.languageID = CountryLanguages.languageID join Regions ON Countries.countryID = Regions.countryID join Places ON Regions.regionID = Places.regionID where CountryLanguages.countryID = :country_id and CountryLanguages.languageID = :language_id;";

    $stmt = $file_db->prepare($sql);
    $stmt->bindParam(':country_id', $country_id);
    $stmt->bindParam(':language_id', $language_id);
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
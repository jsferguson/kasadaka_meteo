<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 08:05
 */
include('get_db.php');

function getLogData(){

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=data.csv');

    $output = fopen('php://output', 'w');

    fputcsv($output, array('timeStamp', 'language', 'country', 'region', 'place', 'days'));

    try {


        $file_db = getDB();
        $stmt = $file_db->query("select timeStamp, language, country, region, place, days from CallLogsView;");

        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($data as $row){

            fputcsv($output, $row);
        }


    }
    catch (PDOException $e) {

        echo $e;
    }

}

getLogData();

<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 16/05/16
 * Time: 09:04
 */
include('get_db.php');

$languageCode = $_POST['languageCode'];
$audiofiles = [];

function getLanguageID($languageCode){

    try {


        $file_db = getDB();
        $stmt = $file_db->query("select languageID from Languages where code = :code;");
        $stmt->bindParam(':code', $languageCode);
        $stmt->execute();
        return $stmt->fetchColumn();


    }
    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }
}

function getGeneralAudioFiles(){

    try {


        $file_db = getDB();
        $stmt = $file_db->query("select name from AudioFiles order by name;");

        $stmt->execute();
        return $general_audio_files = $stmt->fetchAll(PDO::FETCH_ASSOC);


    }
    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }


}

function getRegionNames($languageID){

    try {


        $file_db = getDB();
        $stmt = $file_db->query("select name from Regions where countryID in (select countryID from CountryLanguages where languageID = :languageID);");
        $stmt->bindParam(':languageID', $languageID);
        $stmt->execute();
        return $district_audio_files = $stmt->fetchAll(PDO::FETCH_ASSOC);


    }
    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }


}
function getPlaceNames($languageID){

    try {


        $file_db = getDB();
        $stmt = $file_db->query("select name from Places where regionID in (select regionID from Regions where countryID in (select countryID from CountryLanguages where languageID = :languageID));");
        $stmt->bindParam(':languageID', $languageID);
        $stmt->execute();
        return $places_audio_files = $stmt->fetchAll(PDO::FETCH_ASSOC);


    }
    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }


}
function getCountryNames($languageID){

    try {


        $file_db = getDB();
        $stmt = $file_db->query("select name from Countries where countryID in (select countryID from CountryLanguages where languageID = :languageID);");
        $stmt->bindParam(':languageID', $languageID);
        $stmt->execute();
        return $country_audio_files = $stmt->fetchAll(PDO::FETCH_ASSOC);


    }
    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }


}
function getLanguageNames($languageID){

    try {


        $file_db = getDB();
        $stmt = $file_db->query("select name from Languages where languageID = :languageID;");
        $stmt->bindParam(':languageID', $languageID);
        $stmt->execute();
        return $language_audio_files = $stmt->fetchAll(PDO::FETCH_ASSOC);


    }
    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }


}

function gatherAllAudioClipNames($audiofiles, $languageID){

    foreach(getGeneralAudioFiles() as $audioFile){

        array_push($audiofiles, $audioFile);
    }
    foreach(getRegionNames($languageID) as $region){

        array_push($audiofiles, $region);
    }
    foreach(getPlaceNames($languageID) as $place){

        array_push($audiofiles, $place);
    }
    foreach(getCountryNames($languageID) as $country){

        array_push($audiofiles, $country);
    }
    foreach(getLanguageNames($languageID) as $language){

        array_push($audiofiles, $language);
    }
    return $audiofiles;
}

function produceMissingFiles($audiofiles, $languge_code){

    $missing_audio_files = [];
    foreach($audiofiles as $audiofile){

        $filename = '/var/www/html/'.$languge_code.'/'.$audiofile['name'].'.wav';
        if(!file_exists($filename)) {

            array_push($missing_audio_files, $audiofile);
        }
    }
    return $missing_audio_files;
}

$languageID = getLanguageID($languageCode);

$audio_files = gatherAllAudioClipNames($audiofiles, $languageID);
$missing_audio_files = produceMissingFiles($audio_files, $languageCode);
echo json_encode($missing_audio_files);




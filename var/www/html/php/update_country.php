<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:13
 */
include('get_db.php');

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data


if (empty($_POST['name']))
    $errors['name'] = 'Name is required.';
if (empty($_POST['code']))
    $errors['code'] = 'Code is required.';
if (empty($_POST['timezone']))
    $errors['timezone'] = 'Timezone is required.';
if (empty($_POST['coountryID']))
    $errors['countryID'] = 'CountryID is required.';

if ( ! empty($errors)) {
    $data['success'] = false;
    $data['errors']  = $errors;
} else {

    $data['success'] = true;
    $data['message'] = 'Succeeded';
}

insertData();
function insertData() {

    $file_db = getDB();
    $insert = "UPDATE Countries SET name = :name, code = :code, timezone = :timezone WHERE  countryID = :countryID";
    $stmt = $file_db->prepare($insert);

    $stmt->bindValue(':name', $_POST['name'], PDO::PARAM_STR);
    $stmt->bindValue(':timezone', $_POST['timezone'], PDO::PARAM_STR);
    $stmt->bindValue(':code', $_POST['code'], PDO::PARAM_STR);
    $stmt->bindValue(':countryID', $_POST['countryID'], PDO::PARAM_INT);



    try {
        $stmt->execute();
        $file_db = null;
    }

    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }
}
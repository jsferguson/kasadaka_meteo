<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:13
 */
include('get_db.php');

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data


if (empty($_POST['name']))
    $errors['name'] = 'Name is required.';
if (empty($_POST['code']))
    $errors['code'] = 'Code is required.';
if (empty($_POST['languageID']))
    $errors['languageID'] = 'LanguageID is required.';

if ( ! empty($errors)) {
    $data['success'] = false;
    $data['errors']  = $errors;
} else {

    $data['success'] = true;
    $data['message'] = 'Succeeded';
}

insertData();
updateLanguageFolder();
function updateLanguageFolder(){

    rename('/var/www/html/'.$_POST['old_code'], '/var/www/html/'.$_POST['code']);
}
function insertData() {

    $file_db = getDB();
    $insert = "UPDATE Languages SET name = :name, code = :code WHERE  languageID = :languageID";
    $stmt = $file_db->prepare($insert);

    $stmt->bindValue(':name', $_POST['name'], PDO::PARAM_STR);
    $stmt->bindValue(':languageID', $_POST['languageID'], PDO::PARAM_INT);
    $stmt->bindValue(':code', $_POST['code'], PDO::PARAM_STR);


    try {
        $stmt->execute();
        $file_db = null;
    }

    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }
}
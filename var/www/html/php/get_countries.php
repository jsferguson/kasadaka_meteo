<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 08:05
 */
include('get_db.php');

try {


    $file_db = getDB();
    $stmt = $file_db->query("select countryID, name, code, timezone from Countries order by name;");

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($data);
}
catch (PDOException $e) {

    echo $e;
}
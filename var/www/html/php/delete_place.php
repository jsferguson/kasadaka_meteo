<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:19
 */
include('get_db.php');

$place_id = $_POST['placeID'];
try {
    
    $places = get_place_names($place_id);
    delete_place_files($places);
    deletePlace($place_id);
} catch (Exception $e) {

    echo $e;
}


function deletePlace($place_id) {

    $file_db = getDB();
    $file_db->exec('PRAGMA foreign_keys = ON;');
    
    $delete = "DELETE FROM Places WHERE placeID = :place_id";
    $stmt = $file_db->prepare($delete);


    $stmt->bindParam(':place_id', $place_id);
    $stmt->execute();
    $file_db = null;
}

function get_place_names($place_id){


    $file_db = getDB();
    $sql = "select Languages.code as language_code, Places.name as place_name 
      from Countries join CountryLanguages ON Countries.countryID = CountryLanguages.countryID 
      join Languages ON Languages.languageID = CountryLanguages.languageID 
      join Regions ON Countries.countryID = Regions.countryID 
      join Places ON Regions.regionID = Places.regionID where Places.placeID = :place_id;";

    $stmt = $file_db->prepare($sql);
    $stmt->bindParam(':place_id', $place_id);
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function delete_place_files($places){

        foreach ($places as $place_name){

            $filename = '/var/www/html/'.$place_name['language_code'].'/'.$place_name['place_name'].'.wav';
            if(file_exists($filename)){

                unlink($filename);
            }
        }
}
<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:13
 */
include('get_db.php');

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data


if (empty($_POST['regionID']))
    $errors['regionID'] = 'RegionID is required.';
if (empty($_POST['name']))
    $errors['name'] = 'name is required.';
if (empty($_POST['countryID']))
    $errors['countryID'] = 'CountryID is required.';


if ( ! empty($errors)) {
    $data['success'] = false;
    $data['errors']  = $errors;
} else {

    $data['success'] = true;
    $data['message'] = 'Succeeded';
}

insertData();
function insertData() {

    $file_db = getDB();
    $insert = "UPDATE Regions SET name = :name, countryID = :countryID WHERE  regionID = :regionID";
    $stmt = $file_db->prepare($insert);

    $stmt->bindValue(':name', $_POST['name'], PDO::PARAM_STR);
    $stmt->bindValue(':regionID', $_POST['regionID'], PDO::PARAM_INT);
    $stmt->bindValue(':countryID', $_POST['countryID'], PDO::PARAM_INT);



    try {
        $stmt->execute();
        $file_db = null;
    }

    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }
}
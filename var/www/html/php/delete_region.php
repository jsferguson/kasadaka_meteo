<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:19
 */
include('get_db.php');

try {
    $region_id = $_POST['regionID'];
    $place_names = get_place_names($region_id);
    delete_place_files($place_names);
    deleteRegion($region_id);
} catch (Exception $e) {

    echo $e;
}



function deleteRegion($region_id) {

    $file_db = getDB();

    $delete = "DELETE FROM Regions WHERE regionID = :region_id";
    $file_db->exec('PRAGMA foreign_keys = ON;');
    $stmt = $file_db->prepare($delete);
    
    $stmt->bindParam(':region_id',$region_id);
    $stmt->execute();
    $file_db = null;

}


function delete_place_files($places){

    foreach ($places as $place_name){

        $filename = '/var/www/html/'.$place_name['language_code'].'/'.$place_name['place_name'].'.wav';
        if(file_exists($filename)){

            unlink($filename);
        }
        $filename = '/var/www/html/'.$place_name['language_code'].'/'.$place_name['region_name'].'.wav';
        if(file_exists($filename)){

            unlink($filename);
        }
    }
}

function get_place_names($region_id){


    $file_db = getDB();
    $sql = "select Languages.code as language_code, Countries.code as country_code, Regions.name as region_name, Places.name as place_name from Countries join CountryLanguages ON Countries.countryID = CountryLanguages.countryID join Languages ON Languages.languageID = CountryLanguages.languageID join Regions ON Countries.countryID = Regions.countryID join Places ON Regions.regionID = Places.regionID where Regions.regionID = :region_id;";

    $stmt = $file_db->prepare($sql);

    $stmt->bindParam(':region_id', $region_id);
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
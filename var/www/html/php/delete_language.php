<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:19
 */
include('get_db.php');

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data


if (empty($_POST['languageID']))
    $errors['languageID'] = 'LanguageID is required.';

if ( ! empty($errors)) {

    $data['success'] = false;
    $data['errors']  = $errors;
} else {


    $data['success'] = true;
    $data['message'] = 'Succeeded';
}


deleteData();
deleteLanguageFolder();
echo json_encode($data);

function deleteLanguageFolder(){

    rmdir('/var/www/html/'.$_POST['code']);
}

function deleteData() {

    $file_db = getDB();
    $file_db->exec('PRAGMA foreign_keys = ON;');

    $delete = "DELETE FROM Languages WHERE languageID = :languageID";
    $stmt = $file_db->prepare($delete);


    $stmt->bindParam(':languageID', $_POST['languageID']);



    try {
        $stmt->execute();
        $file_db = null;
    }


    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }
}
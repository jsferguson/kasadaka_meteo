<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 15/05/16
 * Time: 09:13
 */
include('get_db.php');

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data


if (empty($_POST['regionID']))
    $errors['regionID'] = 'RegionID is required.';
if (empty($_POST['name']))
    $errors['name'] = 'name is required.';
if (empty($_POST['placeID']))
    $errors['placeID'] = 'PlaceID is required.';
if (empty($_POST['lattitude']))
    $errors['lattitude'] = 'Latitude is required.';
if (empty($_POST['lattitude']))
    $errors['longtitude'] = 'Longtitude is required.';


if ( ! empty($errors)) {
    $data['success'] = false;
    $data['errors']  = $errors;
} else {

    $data['success'] = true;
    $data['message'] = 'Succeeded';
}

insertData();
function insertData() {

    $file_db = getDB();
    $insert = "UPDATE Places SET name = :name, placeID = :placeID, regionID=:regionID, lattitude=:lattitude, longtitude=:longtitude WHERE  placeID = :placeID";
    $stmt = $file_db->prepare($insert);

    $stmt->bindValue(':name', $_POST['name'], PDO::PARAM_STR);
    $stmt->bindValue(':regionID', $_POST['regionID'], PDO::PARAM_INT);
    $stmt->bindValue(':placeID', $_POST['placeID'], PDO::PARAM_INT);
    $stmt->bindValue(':lattitude', $_POST['lattitude'], PDO::PARAM_STR);
    $stmt->bindValue(':longtitude', $_POST['longtitude'], PDO::PARAM_STR);



    try {
        $stmt->execute();
        $file_db = null;
    }

    catch (PDOException $e) {
        $data['success'] = false;
        $data['message'] = 'Failed!';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jari
 * Date: 24/04/16
 * Time: 07:11
 */


class Country{

    var $countryID;
    var $name;
    var $code;
    var $timezone;

    function __construct($in_countryID, $in_name, $in_code, $in_timezone) {

        $this->countryID = $in_countryID;
        $this->name = $in_name;
        $this->code = $in_code;
        $this->timezone = $in_timezone;
        date_default_timezone_set($this->timezone);
    }
}

class Language {

    var $languageID;
    var $name;
    var $code;

    function __construct($in_languageID, $in_name, $in_code) {

        $this->languageID = $in_languageID;
        $this->name = $in_name;
        $this->code = $in_code;
    }
}

class Region {

    var $regionID;
    var $regionName;

    function __construct($in_regionID, $in_regionName)
    {

        $this->regionID = $in_regionID;
        $this->regionName = $in_regionName;
    }

}

class Place {

    var $placeID;
    var $name;
    var $lattitude;
    var $longtitude;
    var $lastUpdateDT;

    function __construct($in_placeID, $in_name, $in_lattitude, $in_longtitude, $in_lastUpdateDT) {

        $this->placeID = $in_placeID;
        $this->name = $in_name;
        $this->lattitude = $in_lattitude;
        $this->longtitude = $in_longtitude;
        $this->lastUpdateDT = $in_lastUpdateDT;
    }

}

class Prediction {

    var $predictionID;
    var $placeID;
    var $dt;
    var $temp;
    var $rain;
    var $windSpeed;
    var $windDegrees;

    function date(){

        $date = date('Y-m-d H:i',$this->dt);
        return $date;

    }

}



class VerbalPrediction{

    var $dt;
    var $morning_1_temp;
    var $morning_2_temp;
    var $morning_1_windSpeed;
    var $morning_2_windSpeed;
    var $morning_1_windDirection;
    var $morning_2_windDirection;
    var $morning_1_rain;
    var $morning_2_rain;
    var $night_1_temp;
    var $night_2_temp;
    var $night_1_windSpeed;
    var $night_2_windSpeed;
    var $night_1_windDirection;
    var $night_2_windDirection;
    var $night_1_rain;
    var $night_2_rain;
    var $afternoon_1_temp;
    var $afternoon_2_temp;
    var $afternoon_1_windSpeed;
    var $afternoon_2_windSpeed;
    var $afternoon_1_windDirection;
    var $afternoon_2_windDirection;
    var $afternoon_1_rain;
    var $afternoon_2_rain;
    var $evening_1_temp;
    var $evening_2_temp;
    var $evening_1_windSpeed;
    var $evening_2_windSpeed;
    var $evening_1_windDirection;
    var $evening_2_windDirection;
    var $evening_1_rain;
    var $evening_2_rain;

    private function getRainForecast($rain1, $rain2, $rain3, $rain4){

        $rainAverage = null;
        if (is_null($rain1) & is_null($rain2) & is_null($rain3)  & is_null($rain4)) {

            $rainAverage = null;
        }
        elseif (is_null($rain1) & is_null($rain2) & is_null($rain3)  & !is_null($rain4)){

            $rainAverage = $rain4;
        }
        elseif (is_null($rain1) & is_null($rain2) & !is_null($rain3)  & !is_null($rain4)){

            $rainAverage = (($rain3 + $rain4)/2.0);
        }
        elseif (is_null($rain1) & !is_null($rain2) & !is_null($rain3)  & !is_null($rain4)){

            $rainAverage = (($rain2 + $rain3 + $rain4)/3.0);
        }

        else {

            $rainAverage = (($rain1 + $rain2 + $rain3 + $rain4)/4.0);
        }

        if(is_null($rainAverage)){

            return null;
        }
        else {

            if($rainAverage <= 0.0){

                return "no_rain.wav";
            }
            elseif ($rainAverage > 0.0 & $rainAverage <= 3.0) {

                return "light_rain.wav";
            }
            elseif ($rainAverage > 3.0 & $rainAverage <= 10.0 ) {

                return "moderate_rain.wav";
            }

            else {

                return "intense_rain.wav";
            }
        }

    }
    private function getTemperatureForecast($temp1, $temp2, $temp3, $temp4){

        $tempAverage = null;
        if (is_null($temp1) & is_null($temp2) & is_null($temp3) & is_null($temp4)) {

            $tempAverage = null;
        }
        elseif (is_null($temp1) & is_null($temp2) & is_null($temp3) & !is_null($temp4)){

            $tempAverage = $temp4;
        }
        elseif (is_null($temp1) & is_null($temp2) & !is_null($temp3) & !is_null($temp4)){

            $tempAverage = (($temp3 + $temp4)/2.0);
        }
        elseif (is_null($temp1) & !is_null($temp2) & !is_null($temp3) & !is_null($temp4)){

            $tempAverage = (($temp2 + $temp3 + $temp4)/3.0);
        }
        else {

            $tempAverage = (($temp1 + $temp2 + $temp3 + $temp4)/4.0);
        }

        if(is_null($tempAverage)){

            return null;
        }
        /*
         * 40+ very hot (tres chaud)
         * 35 - 39 hot (chaud)
         * 25-34 average (moyen)
         * 17-24 cold (frais)
         * < 16 very cold (tres frais)
         */
        else {

           if ($tempAverage <= 289.15){

               return "freezing.wav";
           }
            elseif ($tempAverage > 289.15 & $tempAverage <= 297.15 ){

                return "cold.wav";
            }
            elseif ($tempAverage > 297.15 & $tempAverage < 307.15) {

                return "warm.wav";
            }
           elseif ($tempAverage > 307.15 & $tempAverage < 312.15) {

               return "hot.wav";
           }
            else {

                return "very_hot.wav";
            }
        }

    }
    private function getWindDirectionForecast($windDirection1, $windDirection2, $windDirection3, $windDirection4){


        $windDirectionAvg = null;
        $windDirectionVerbal = null;

        if (is_null($windDirection1) & is_null($windDirection2) & is_null($windDirection3) & is_null($windDirection4)) {

            $windDirectionAvg = null;
            return null;
        }
        elseif (is_null($windDirection1) & is_null($windDirection2) & is_null($windDirection3) & !is_null($windDirection4)){

            $windDirectionAvg = $windDirection4;
        }
        elseif (is_null($windDirection1) & is_null($windDirection2) & !is_null($windDirection3) & !is_null($windDirection4)){

            $windDirectionAvg = (($windDirection3 + $windDirection4)/2.0);
        }
        elseif (is_null($windDirection1) & !is_null($windDirection2) & !is_null($windDirection3) & !is_null($windDirection4)){

            $windDirectionAvg = (($windDirection2 + $windDirection3 + $windDirection4)/3.0);
        }
        else {

            $windDirectionAvg = (($windDirection1 + $windDirection2 + $windDirection3 + $windDirection4)/4.0);
        }

        if ($windDirectionAvg > 0 & $windDirectionAvg <= 45.0){

            $windDirectionVerbal = "north.wav";
        }
        elseif ($windDirectionAvg > 45.0 & $windDirectionAvg <= 90.0 ) {

            $windDirectionVerbal = "north_east.wav";
        }
        elseif ($windDirectionAvg > 90.0 & $windDirectionAvg <= 135) {

            $windDirectionVerbal = "east.wav";
        }
        elseif ($windDirectionAvg > 135.0 & $windDirectionAvg <= 180.0) {

            $windDirectionVerbal = "south_east.wav";
        }
        elseif ($windDirectionAvg > 180 & $windDirectionAvg <= 225.0) {

            $windDirectionVerbal = "south.wav";
        }
        elseif ($windDirectionAvg > 225.0 & $windDirectionAvg <= 270.0){

            $windDirectionVerbal = "south_west.wav";
        }
        elseif ($windDirectionAvg > 270.0 & $windDirectionAvg <= 315.0){

            $windDirectionVerbal = "west.wav";
        }
        else {

            $windDirectionVerbal = "north.wav";
        }

        return $windDirectionVerbal;


    }
    private function getWindSpeedForecast($windSpeed1, $windSpeed2, $windSpeed3, $windSpeed4){

        $windSpeedAvg = null;
        $windSpeedVerbal = null;
        if (is_null($windSpeed1) & is_null($windSpeed2) & is_null($windSpeed3) & is_null($windSpeed4)) {

            $windSpeedAvg = null;

            return null;
        }
        elseif (is_null($windSpeed1) & is_null($windSpeed2) & is_null($windSpeed3) & !is_null($windSpeed4)){

            $windSpeedAvg = $windSpeed4;

        }
        elseif (is_null($windSpeed1) & is_null($windSpeed2) & !is_null($windSpeed3) & !is_null($windSpeed4)){

            $windSpeedAvg = (($windSpeed3 + $windSpeed4)/ 2.0);

        }
        elseif (is_null($windSpeed1) & !is_null($windSpeed2) & !is_null($windSpeed3) & !is_null($windSpeed4)){

            $windSpeedAvg = (($windSpeed2 + $windSpeed3 + $windSpeed4)/ 3.0);

        }
        else {

            $windSpeedAvg = (($windSpeed1 + $windSpeed2 + $windSpeed3 + $windSpeed4)/ 4.0);

        }
        if ($windSpeedAvg >= 0.0 & $windSpeedAvg <= 1.9){

            // 1 Beaufort
            $windSpeedVerbal = "light_air.wav";
        }
        elseif ($windSpeedAvg > 1.9 & $windSpeedAvg <= 3.3) {

            // 2 Beaufort
            $windSpeedVerbal = "light_breeze.wav";

        }
        elseif ($windSpeedAvg > 3.3 & $windSpeedAvg <= 5.4) {

            // 3 Beaufort
            $windSpeedVerbal = "gentle_breeze.wav";

        }
        elseif ($windSpeedAvg > 5.4 & $windSpeedAvg <= 7.9) {

            // 4 Beaufort
            $windSpeedVerbal = "breeze.wav";

        }
        elseif ($windSpeedAvg > 7.9 & $windSpeedAvg <= 11.0) {

            // 5 Beaufort
            $windSpeedVerbal = "fresh_breeze.wav";

        }
        elseif ($windSpeedAvg > 11.0 & $windSpeedAvg <= 14.1) {

            // 6 Beaufort
            $windSpeedVerbal = "strong_breeze.wav";

        }
        elseif ($windSpeedAvg > 14.1 & $windSpeedAvg <= 17.2) {

            // 7 Beaufort
            $windSpeedVerbal = "near_gale.wav";

        }
        elseif ($windSpeedAvg > 17.2& $windSpeedAvg <= 20.8) {

            // 8 Beaufort
            $windSpeedVerbal = "gale.wav";

        }
        elseif ($windSpeedAvg > 20.8 & $windSpeedAvg <= 24.4) {

            // 9 Beaufort
            $windSpeedVerbal = "strong_gale.wav";

        }
        elseif ($windSpeedAvg > 24.4 & $windSpeedAvg <= 28.5) {

            // 10 Beaufort
            $windSpeedVerbal = "storm.wav";

        }
        elseif ($windSpeedAvg > 28.5 & $windSpeedAvg <= 32.6) {

            // 11 Beaufort
            $windSpeedVerbal = "violent_storm.wav";

        }
        else {

            // 12 Beaufort
            $windSpeedVerbal = "hurricane.wav";

        }



        return $windSpeedVerbal;

    }

    /*
    function getRainForecastMorning(){

        return $this->getRainForecast($this->morning_1_rain, $this->morning_1_rain);
    }
    */
    function getRainForecastAfternoon(){

        return $this->getRainForecast($this->afternoon_1_rain, $this->afternoon_2_rain, $this->evening_1_rain, $this->evening_2_rain);
    }
    /*
    function getRainForecastEvening(){

        return $this->getRainForecast($this->evening_1_rain, $this->evening_2_rain);
    }
    */
    function getRainForecastNight(){

        return $this->getRainForecast($this->night_1_rain, $this->night_2_rain, $this->morning_1_rain, $this->morning_2_rain);
    }
    /*
    function getTemperatureForecastMorning(){

        return $this->getTemperatureForecast($this->morning_1_temp, $this->morning_2_temp);
    }
    */
    function getTemperatureForecastAfternoon(){

        return $this->getTemperatureForecast($this->afternoon_1_temp, $this->afternoon_2_temp, $this->evening_1_temp, $this->evening_2_temp);
    }
    /*
    function getTemperatureForecastEvening(){

        return $this->getTemperatureForecast($this->evening_1_temp, $this->evening_2_temp);
    }
    */
    function getTemperatureForecastNight(){

        return $this->getTemperatureForecast($this->night_1_temp, $this->night_2_temp, $this->morning_1_temp, $this->morning_2_temp);
    }

    /*
    function getWindSpeedForecastMorning(){

        return $this->getWindSpeedForecast($this->morning_1_windSpeed, $this->morning_2_windSpeed);
    }
    */
    function getWindSpeedForecastAfternoon(){

        return $this->getWindSpeedForecast($this->afternoon_1_windSpeed, $this->afternoon_2_windSpeed, $this->evening_1_windSpeed, $this->evening_2_windSpeed);
    }
    /*
    function getWindSpeedForecastEvening(){

        return $this->getWindSpeedForecast($this->evening_1_windSpeed, $this->evening_2_windSpeed);
    }
    */
    function getWindSpeedForecastNight(){

        return $this->getWindSpeedForecast($this->night_1_windSpeed, $this->night_2_windSpeed, $this->morning_1_windSpeed, $this->morning_2_windSpeed);
    }

    /*
    function getWindDirectionForecastMorning(){

        return $this->getWindDirectionForecast($this->morning_1_windDirection, $this->morning_2_windDirection);
    }
    */
    function getWindDirectionForecastAfternoon(){

        return $this->getWindDirectionForecast($this->afternoon_1_windDirection, $this->afternoon_2_windDirection, $this->evening_1_windDirection, $this->evening_2_windDirection);
    }
    /*
    function getWindDirectionForecastEvening(){

        return $this->getWindDirectionForecast($this->evening_1_windDirection, $this->evening_2_windDirection);
    }
    */
    function getWindDirectionForecastNight(){

        return $this->getWindDirectionForecast($this->night_1_windDirection, $this->night_2_windDirection, $this->morning_1_windDirection, $this->morning_2_windDirection);
    }

}

class Main {

    var $listOfCountries = array();
    var $listOfPlaces = array();
    var $listOfPredictions = array();
    var $listOfVerbalPredictions = array();
    var $listOfLanguages = array();
    var $listOfRegions = array();
    var $language;
    var $country;
    var $region;
    var $place;
    var $days;
    var $xml;
    var $isOffline;
    var $APIkey;
    var $systemCountry;

    var $numberWAVES = array();
    var $monthsWAVES = array();
    var $daysWAVES = array();

    function __construct() {

        $this->xml = new XMLWriter();
        $this->xml->openURI('php://output');
        $this->xml->startDocument('1.0');
        $this->xml->setIndent(4);
        $this->xml->startElement('vxml');
        $this->xml->writeAttribute('version', '2.0');
        $this->buildNumberWAVES();
        $this->buildMonthsWAVES();
        $this->buildDaysWAVES();
        $this->readConfigFile();
    }
    function buildNumberWAVES() {

        $this->numberWAVES = ['one.wav','two.wav','three.wav','four.wav','five.wav','six.wav','seven.wav','eight.wav',
            'nine.wav','ten.wav','eleven.wav','twelve.wav','thirteen.wav','fourteen.wav','fifteen.wav','sixteen.wav',
            'seventeen.wav','eighteen.wav','nineteen.wav','twenty.wav','twentyone.wav','twentytwo.wav',
            'twentythree.wav','twentyfour.wav','twentyfive.wav','twentysix.wav','twentyseven.wav','twentyeight.wav',
            'twentynine.wav','thirty.wav','thirtyone.wav'];
    }
    function buildMonthsWAVES(){

        $this->monthsWAVES = ['january.wav','february.wav','march.wav','april.wav','may.wav','june.wav','july.wav',
        'august.wav','september.wav','october.wav','november.wav','december.wav'];

    }
    function buildDaysWAVES(){

        $this->daysWAVES = ['monday.wav','tuesday.wav','wednesday.wav','thursday.wav','friday.wav','saturday.wav',
            'sunday.wav'];
    }
    function flushXML(){

        $this->xml->endElement();
        $this->xml->endElement();
        $this->xml->endDocument();
        $this->xml->flush();
    }
    function getTodaysDate(){

        $dr= date_create_from_format('Ymd',  (date(Ymd)));
        return $dr->format('l j F');
    }
    function getDB(){

        $file_db = new PDO('sqlite:./db/meteo.sqlite3');
        $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $file_db;
    }
    function getCountryByIdFromDB($in_countryID){

        try {

            $file_db = $this->getDB();
            $stmt = $file_db->query("SELECT countryID, name, code, timezone FROM Countries WHERE countryID = :countryID");
            $stmt->bindParam(':countryID', $in_countryID, PDO::PARAM_INT);
            $stmt->execute();
            $queryResponse = $stmt->fetch(PDO::FETCH_ASSOC);
            $file_db= null;
            return new Country($queryResponse['countryID'], $queryResponse['name'], $queryResponse['code'], $queryResponse['gmtOffset']);

        }
        catch (PDOException $e) {

            echo $e;
        }
    }
    function getRegionByIdFromDB($in_regionID){
        try {

            $file_db = $this->getDB();
            $stmt = $file_db->query("SELECT regionID, name FROM Regions WHERE regionID = :regionID");
            $stmt->bindParam(':regionID', $in_regionID, PDO::PARAM_INT);
            $stmt->execute();
            $queryResponse = $stmt->fetch(PDO::FETCH_ASSOC);
            $file_db= null;
            return new Region($queryResponse['regionID'], $queryResponse['name']);

        }
        catch (PDOException $e) {

            echo $e;
        }

    }
    function getPlacesByRegiondID(){

        try {

            $listOfPlaces = array();
            $file_db = $this->getDB();
            $stmt = $file_db->query("SELECT placeID, name, lattitude, longtitude, lastUpdateDT FROM Places WHERE regionID = :regionID");
            $stmt->bindParam(':regionID', $this->region->regionID, PDO::PARAM_INT);
            $stmt->execute();
            $queryResponse = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $file_db= null;
            foreach ($queryResponse as $place){

                array_push($listOfPlaces, new Place($place['placeID'], $place['name'], $place['lattitude'], $place['longtitude'],$place['lastUpdateDT']));
            }
            return $listOfPlaces;

        }
        catch (PDOException $e) {

            echo $e;
        }
    }
    function getPlaceByPlaceID($in_placeID){

        try {

            $file_db = $this->getDB();
            $stmt = $file_db->query("SELECT placeID, name, lattitude, longtitude, lastUpdateDT FROM Places WHERE placeID = :placeID");
            $stmt->bindParam(':placeID', $in_placeID, PDO::PARAM_INT);
            $stmt->execute();
            $queryResponse = $stmt->fetch(PDO::FETCH_ASSOC);
            $file_db= null;
            return new Place($queryResponse['placeID'], $queryResponse['name'],$queryResponse['lattitude'],$queryResponse['longtitude'],$queryResponse['lastUpdateDT']);

        }
        catch (PDOException $e) {

            echo $e;
        }
    }
    function getLanguageByLanguageID($in_languageID){

        try {

            $file_db = $this->getDB();
            $stmt = $file_db->query("SELECT languageID, name, code FROM Languages WHERE languageID = :languageID");
            $stmt->bindParam(':languageID', $in_languageID, PDO::PARAM_INT);
            $stmt->execute();
            $queryResponse = $stmt->fetch(PDO::FETCH_ASSOC);
            $file_db= null;
            return new  Language($queryResponse['languageID'], $queryResponse['name'],$queryResponse['code']);

        }
        catch (PDOException $e) {

            echo $e;
        }
    }
    function getRegions(){

        try {

            $listOfRegions = array();
            $file_db = $this->getDB();
            $stmt = $file_db->query("SELECT regionID, name FROM Regions WHERE countryID = :countryID");
            $stmt->bindParam(':countryID', $this->country->countryID, PDO::PARAM_INT);
            $stmt->execute();
            $queryResponse = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $file_db= null;
            foreach ($queryResponse as $region){

                array_push($listOfRegions, new Region($region['regionID'], $region['name']));
            }
            return $listOfRegions;

        }
        catch (PDOException $e) {

            echo $e;
        }
    }
    function getLanguages($in_countryID){

        try {

            $file_db = $this->getDB();
            //$stmt = $file_db->query("SELECT languageID, name, code FROM Languages");
            $stmt = $file_db->query("SELECT Languages.languageID, Languages.name, Languages.code FROM Languages inner join CountryLanguages on CountryLanguages.languageID=Languages.languageID where CountryLanguages.countryID = :countryID;");
            $stmt->bindParam(':countryID', $in_countryID, PDO::PARAM_INT);
            $stmt->execute();
            $queryResponse = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $file_db= null;
            foreach ($queryResponse as $language){

                array_push($this->listOfLanguages, new Language($language['languageID'], $language['name'], $language['code']));
            }

        }
        catch (PDOException $e) {

            echo $e;
        }
    }
    function timeGreeting(){

        date_default_timezone_set($this->country->timezone);
        //date_default_timezone_set('Africa/Bamako');
        $current_time = date('H') ;
        if ($current_time >= 6 & $current_time < 12){

            return "goodmorning.wav";
        }
        elseif ($current_time >= 12 & $current_time <17) {

            return "goodafternoon.wav";
        }
        elseif ($current_time >= 17 & $current_time <20){

            return "goodevening.wav";
        }
        else
            return "goodnight.wav";
    }
    function vxmlWelcome(){

        $this->xml->startElement('property');
        $this->xml->writeAttribute('name','inputmodes');
        $this->xml->writeAttribute('value','dtmf');
        $this->xml->endElement();

        $this->xml->startElement('form');
        $this->xml->writeAttribute('id','menu');
        $this->xml->startElement('field');
        $this->xml->writeAttribute('name','question1');

        $this->xml->startElement('prompt');
        $this->say($this->language->code.'/'.$this->timeGreeting());
        $this->say($this->language->code.'/this_is_weather_service_for.wav');
        $this->say($this->language->code.'/'.$this->country->name.'.wav');
        $this->say($this->language->code.'/'.'the_date_is.wav');
        $time = strtotime($this->getTodaysDate());
        $day = date('d',$time);
        $month = date('m',$time);

        $this->say($this->language->code.'/'.$this->numberWAVES[$day-1]);
        $this->say($this->language->code.'/'.$this->monthsWAVES[$month-1]);
        $this->say($this->language->code.'/'.'select_a_region.wav');

        $counter = 0;
        foreach ($this->getRegions() as $region){

            $this->say('/'.$this->language->code.'/select.wav');
            $this->say('/'.$this->language->code.'/'.$this->numberWAVES[$counter]);
            $this->say('/'.$this->language->code.'/for.wav');
            $this->say('/'.$this->language->code.'/'.$region->regionName.'.wav');

            $counter++;
        }
        $this->xml->endElement(); // end prompt

        // start grammar
        $this->xml->startElement('grammar');
        $this->xml->writeAttribute('xml:lang','en-US');
        $this->xml->writeAttribute('root','MYRULE');
        $this->xml->writeAttribute('mode','dtmf');
        $this->xml->startElement('rule');
        $this->xml->writeAttribute('id','MYRULE');
        $this->xml->writeAttribute('scope','public');
        $this->xml->startElement('one-of');
        $counter = 1;
        foreach ($this->getRegions() as $region){

            $this->xml->startElement('item');
            $this->xml->text($counter);
            $this->xml->endElement(); // end item
            $counter++;

        }
        $this->xml->endElement(); // end one-of


        $this->xml->endElement(); // end rule

        $this->xml->endElement(); // end grammar

        $this->xml->startElement('filled');
        $this->listOfRegions = $this->getRegions();
        $region = $this->listOfRegions[0];
        $this->xml->startElement('if');
        $this->xml->writeAttribute("cond","question1 == '1'");
        $this->xml->startElement('goto');
        $this->xml->writeAttribute('next','forecast.php?country='.$this->country->countryID.'&language='.$this->language->languageID.'&region'.'='.$region->regionID);
        $this->xml->endElement(); // end goto


        for ($i = 1; $i < count($this->listOfRegions); $i++){

            $region = $this->listOfRegions[$i];
            $this->xml->startElement('elseif');
            $this->xml->writeAttribute("cond","question1 == '".($i+1)."'");
            $this->xml->endElement(); // end elseif
            $this->xml->startElement('goto');
            $this->xml->writeAttribute('next','forecast.php?country='.$this->country->countryID.'&language='.$this->language->languageID.'&region'.'='.$region->regionID);
            $this->xml->endElement(); // end goto
            //$this->xml->endElement(); // end elseif
        }
        $this->xml->startElement('else');
        $this->xml->endElement(); // end else
        $this->xml->endElement(); // end if

    }
    function vxmlSelectPlace(){

        $this->xml->startElement('property');
        $this->xml->writeAttribute('name','inputmodes');
        $this->xml->writeAttribute('value','dtmf');
        $this->xml->endElement();

        $this->xml->startElement('form');
        $this->xml->writeAttribute('id','menu');
        $this->xml->startElement('field');
        $this->xml->writeAttribute('name','question1');

        $this->xml->startElement('prompt');
        $this->say('/'.$this->language->code.'/'.'please_select_a_place.wav');

        $counter = 0;
        foreach ($this->getPlacesByRegiondID() as $place){

            $this->say('/'.$this->language->code.'/select.wav');
            $this->say('/'.$this->language->code.'/'.$this->numberWAVES[$counter]);
            $this->say('/'.$this->language->code.'/for.wav');
            $this->say('/'.$this->language->code.'/'.$place->name.'.wav');

            $counter++;
        }

        $this->xml->endElement(); // end prompt


        // start grammar
        $this->xml->startElement('grammar');
        $this->xml->writeAttribute('xml:lang','en-US');
        $this->xml->writeAttribute('root','MYRULE');
        $this->xml->writeAttribute('mode','dtmf');
        $this->xml->startElement('rule');
        $this->xml->writeAttribute('id','MYRULE');
        $this->xml->writeAttribute('scope','public');
        $this->xml->startElement('one-of');
        $counter = 1;
        foreach ($this->getPlacesByRegiondID() as $place){

            $this->xml->startElement('item');
            $this->xml->text($counter);
            $this->xml->endElement(); // end item
            $counter++;

        }
        $this->xml->endElement(); // end one-of


        $this->xml->endElement(); // end rule

        $this->xml->endElement(); // end grammar


        $this->xml->startElement('filled');
        $this->listOfPlaces = $this->getPlacesByRegiondID();
        $place = $this->listOfPlaces[0];
        $this->xml->startElement('if');
        $this->xml->writeAttribute("cond","question1 == '1'");
        $this->xml->startElement('goto');
        $this->xml->writeAttribute('next','forecast.php?country='.$this->country->countryID.'&language='.$this->language->languageID.'&region'.'='.$this->region->regionID.'&place='.$place->placeID);
        $this->xml->endElement(); // end goto


        for ($i = 1; $i < count($this->listOfPlaces); $i++){

            $place = $this->listOfPlaces[$i];
            $this->xml->startElement('elseif');
            $this->xml->writeAttribute("cond","question1 == '".($i+1)."'");
            $this->xml->endElement(); // end elseif
            $this->xml->startElement('goto');
            $this->xml->writeAttribute('next','forecast.php?country='.$this->country->countryID.'&language='.$this->language->languageID.'&region'.'='.$this->region->regionID.'&place='.$place->placeID);
            $this->xml->endElement(); // end goto
            //$this->xml->endElement(); // end elseif
        }
        $this->xml->startElement('else');
        $this->xml->endElement(); // end else
        $this->xml->endElement(); // end if


    }
    function vxmlSelectDays(){

        $this->xml->startElement('property');
        $this->xml->writeAttribute('name','inputmodes');
        $this->xml->writeAttribute('value','dtmf');
        $this->xml->endElement();

        $this->xml->startElement('form');
        $this->xml->writeAttribute('id','menu');
        $this->xml->startElement('field');
        $this->xml->writeAttribute('name','question1');

        $this->xml->startElement('prompt');
        $this->say('/'.$this->language->code.'/'.'you_have_chosen.wav');
        $this->say('/'.$this->language->code.'/'.$this->place->name.'.wav');
        $this->say('/'.$this->language->code.'/'.'please_select_the_number_of_days.wav');

        $this->xml->endElement(); // end prompt


        // start grammar
        $this->xml->startElement('grammar');
        $this->xml->writeAttribute('xml:lang','en-US');
        $this->xml->writeAttribute('root','MYRULE');
        $this->xml->writeAttribute('mode','dtmf');
        $this->xml->startElement('rule');
        $this->xml->writeAttribute('id','MYRULE');
        $this->xml->writeAttribute('scope','public');
        $this->xml->startElement('one-of');
        //$counter = 1;
        for ($i=1; $i<6;$i++){

            $this->xml->startElement('item');
            $this->xml->text($i);
            $this->xml->endElement(); // end item


        }
        $this->xml->endElement(); // end one-of


        $this->xml->endElement(); // end rule

        $this->xml->endElement(); // end grammar


        $this->xml->startElement('filled');
        $this->listOfPlaces = $this->getPlacesByRegiondID();
        $place = $this->listOfPlaces[0];
        $this->xml->startElement('if');
        $this->xml->writeAttribute("cond","question1 == '1'");
        $this->xml->startElement('goto');
        $this->xml->writeAttribute('next','forecast.php?country='.$this->country->countryID.'&language='.$this->language->languageID.'&region'.'='.$this->region->regionID.'&place='.$place->placeID.'&days=1');
        $this->xml->endElement(); // end goto


        for ($i = 2; $i < 6; $i++){

            //$place = $this->listOfPlaces[$i];
            $this->xml->startElement('elseif');
            $this->xml->writeAttribute("cond","question1 == '".($i)."'");
            $this->xml->endElement(); // end elseif
            $this->xml->startElement('goto');
            $this->xml->writeAttribute('next','forecast.php?country='.$this->country->countryID.'&language='.$this->language->languageID.'&region'.'='.$this->region->regionID.'&place='.$place->placeID.'&days='.$i);
            $this->xml->endElement(); // end goto
        }
        $this->xml->startElement('else');
        $this->xml->endElement(); // end else
        $this->xml->endElement(); // end if

    }
    function vxmlSelectLanguage(){

        $this->xml->startElement('property');
        $this->xml->writeAttribute('name','inputmodes');
        $this->xml->writeAttribute('value','dtmf');
        $this->xml->endElement();

        $this->xml->startElement('form');
        $this->xml->writeAttribute('id','menu');
        $this->xml->startElement('field');
        $this->xml->writeAttribute('name','question1');

        $this->xml->startElement('prompt');
        $counter = 0;
        foreach ($this->listOfLanguages as $language){

            $this->say('/'.$language->code.'/select.wav');
            $this->say('/'.$language->code.'/'.$this->numberWAVES[$counter]);
            $this->say('/'.$language->code.'/for.wav');
            $this->say('/'.$language->code.'/'.$language->name.'.wav');

            $counter++;
        }
        $this->xml->endElement(); // end prompt

        // start grammar
        $this->xml->startElement('grammar');
        $this->xml->writeAttribute('xml:lang','en-US');
        $this->xml->writeAttribute('root','MYRULE');
        $this->xml->writeAttribute('mode','dtmf');
        $this->xml->startElement('rule');
        $this->xml->writeAttribute('id','MYRULE');
        $this->xml->writeAttribute('scope','public');
        $this->xml->startElement('one-of');
        $counter = 1;
        foreach ($this->listOfLanguages as $language){

            $this->xml->startElement('item');
            $this->xml->text($counter);
            $this->xml->endElement(); // end item
            $counter++;

        }
        $this->xml->endElement(); // end one-of


        $this->xml->endElement(); // end rule

        $this->xml->endElement(); // end grammar

        $this->xml->startElement('filled');
        $language = $this->listOfLanguages[0];
        $this->xml->startElement('if');
        $this->xml->writeAttribute("cond","question1 == '1'");
        $this->xml->startElement('goto');
        $this->xml->writeAttribute('next','forecast.php?country='.$this->country->countryID.'&language='.$language->languageID);
        $this->xml->endElement(); // end goto


        for ($i = 1; $i < count($this->listOfLanguages); $i++){

            $language = $this->listOfLanguages[$i];
            $this->xml->startElement('elseif');
            $this->xml->writeAttribute("cond","question1 == '".($i+1)."'");
            $this->xml->endElement(); // end elseif
            $this->xml->startElement('goto');
            $this->xml->writeAttribute('next','forecast.php?country='.$this->country->countryID.'&language='.$language->languageID);
            $this->xml->endElement(); // end goto
            //$this->xml->endElement(); // end elseif
        }
        $this->xml->startElement('else');
        $this->xml->endElement(); // end else
        $this->xml->endElement(); // end if


    }
    function vxmlPredictions(){

        $this->xml->startElement('form');
        $this->xml->startElement('block');

        $this->xml->startElement('prompt');
        for ($i = 0; $i<$this->days;$i++) {

            $verbalPrediction = $this->listOfVerbalPredictions[$i];

            $this->say('/'.$this->language->code.'/the_weather_for.wav');

            $day = gmdate("d", $this->listOfVerbalPredictions[$i]->dt);
            $month = gmdate("m", $this->listOfVerbalPredictions[$i]->dt);

            $this->say('/'.$this->language->code.'/'.$this->numberWAVES[$day-1]);
            $this->say('/'.$this->language->code.'/'.$this->monthsWAVES[$month-1]);


            if (!is_null($verbalPrediction->getRainForecastNight())){

                $this->say('/'.$this->language->code.'/in_the_night.wav');
                $this->say('/' .$this->language->code . '/it_will_be.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getTemperatureForecastNight());
                $this->say('/'.$this->language->code.'/with.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getRainForecastNight());
                $this->say('/'.$this->language->code.'/with.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getWindSpeedForecastNight());
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getWindDirectionForecastNight());
                $this->say('/' . $this->language->code . '/BREAK.wav');
            }

            /*
            if (!is_null($verbalPrediction->getRainForecastMorning())) {
                $this->say('/' . $this->language->code . '/in_the_morning.wav');
                $this->say('/' . $this->language->code . '/it_will_be.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getTemperatureForecastMorning());
                $this->say('/'.$this->language->code.'/with.wav');
                $this->say('/' . $this->language->code . '/'.$verbalPrediction->getRainForecastMorning());
                $this->say('/' . $this->language->code . '/with.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getWindSpeedForecastMorning());
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getWindDirectionForecastMorning());
                $this->say('/' . $this->language->code . '/BREAK.wav');

            }
            */

            if (!is_null($verbalPrediction->getRainForecastAfternoon())) {


                $this->say('/' . $this->language->code . '/in_the_afternoon.wav');
                $this->say('/' . $this->language->code . '/it_will_be.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getTemperatureForecastAfternoon());
                $this->say('/'.$this->language->code.'/with.wav');
                $this->say('/' . $this->language->code . '/'.$verbalPrediction->getRainForecastAfternoon());
                $this->say('/' . $this->language->code . '/with.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getWindSpeedForecastAfternoon());
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getWindDirectionForecastAfternoon());
                $this->say('/' . $this->language->code . '/BREAK.wav');


            }
            /*
            if (!is_null($verbalPrediction->getRainForecastEvening())) {

                $this->say('/' . $this->language->code . '/in_the_evening.wav');
                $this->say('/' . $this->language->code . '/it_will_be.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getTemperatureForecastEvening());
                $this->say('/'.$this->language->code.'/with.wav');
                $this->say('/' . $this->language->code . '/'.$verbalPrediction->getRainForecastEvening());
                $this->say('/' . $this->language->code . '/with.wav');
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getWindSpeedForecastEvening());
                $this->say('/'.$this->language->code.'/'.$verbalPrediction->getWindDirectionForecastEvening());
                $this->say('/' . $this->language->code . '/BREAK.wav');

            }
            */
            $this->say('/' . $this->language->code . '/BREAK.wav');
         }

        $this->say('/' . $this->language->code . '/end.wav');
        $this->xml->endElement(); // end prompt
        $this->xml->endElement(); // end block
        $this->xml->endDocument();
        $this->xml->flush();
    }
    function say($in_string){

        $this->xml->startElement('audio');
        $this->xml->writeAttribute('src',$in_string);
        $this->xml->writeAttribute('fetchhint', 'prefetch');
        $this->xml->endElement();
    }
    function setIsOffline($in_isOffline){

        $this->isOffline = $in_isOffline;
    }
    function updateNeeded(){

        if ((time()-$this->place->lastUpdateDT) > 10800 || is_null($this->place->lastUpdate)){

            return true;
        }
        else {

            return false;
        }
    }
    function getOnlineWeatherData(){

        //$url = "http://api.openweathermap.org/data/2.5/forecast?lat=".$this->place->lattitude."&lon=".$this->place->longtitude."&appid=0c8da42072394b8fed95de897726ad2d";
        $url = "http://api.openweathermap.org/data/2.5/forecast?lat=".$this->place->lattitude."&lon=".$this->place->longtitude."&appid=".$this->APIkey;
        //echo $url;
        $data = file_get_contents($url);
        $mod = str_replace("3h", "h3", $data);
        $input = json_decode($mod);
        $predictionList = $input->list;
        foreach ($predictionList as $prediction){

            //__construct($in_predictionID, $in_placeID, $in_dt, $in_temp, $in_rain, $in_windSpeed, $in_windDegrees)
            $newPrediction = new Prediction();
            $newPrediction->placeID = $this->place->placeID;
            $newPrediction->dt = $prediction->dt;
            $newPrediction->temp = $prediction->main->temp;

            if(isset($prediction->rain->h3)){

                $newPrediction->rain = $prediction->rain->h3;
            }
            else {

                $newPrediction->rain = 0.0;
            }

            $newPrediction->windSpeed = $prediction->wind->speed;
            $newPrediction->windDegrees = $prediction->wind->deg;
            array_push($this->listOfPredictions,$newPrediction);
        }
        //echo json_encode($this->listOfPredictions);
    }
    function getOfflineWeatherData(){

        date_default_timezone_set('Africa/Bamako');
        $ncurrentDT = strtotime('today midnight');
        $url = "forecast.json";
        //echo $url;
        $data = file_get_contents($url);
        $mod = str_replace("3h", "h3", $data);
        $input = json_decode($mod);
        $predictionList = $input->list;
        foreach ($predictionList as $prediction){

            //__construct($in_predictionID, $in_placeID, $in_dt, $in_temp, $in_rain, $in_windSpeed, $in_windDegrees)
            $newPrediction = new Prediction();
            $newPrediction->placeID = $this->place->placeID;
            $newPrediction->dt = $ncurrentDT;
            $newPrediction->temp = $prediction->main->temp;

            if(isset($prediction->rain->h3)){

                $newPrediction->rain = $prediction->rain->h3;
            }
            else {

                $newPrediction->rain = 0.0;
            }

            $newPrediction->windSpeed = $prediction->wind->speed;
            $newPrediction->windDegrees = $prediction->wind->deg;
            array_push($this->listOfPredictions,$newPrediction);
            $ncurrentDT = ($ncurrentDT + 10800);
        }
        //echo json_encode($this->listOfPredictions);
    }
    function insertPredictionsIntoDB(){

        $file_db = $this->getDB();
        try {
            $deleteQuery = "DELETE FROM Predictions WHERE placeID = :placeID";
            $stmt = $file_db->prepare($deleteQuery);
            $stmt->bindValue(':placeID', $this->place->placeID, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $e){

            echo $e;
        }
        try {
            $insertQuery = "INSERT INTO Predictions (placeID, dt, temp, rain, windSpeed, windDegrees) VALUES (:placeID, :dt, :temp, :rain, :windSpeed, :windDegrees)";
            $stmt = $file_db->prepare($insertQuery);

            foreach ($this->listOfPredictions as $prediction) {
                $stmt->bindValue(':placeID', $prediction->placeID, PDO::PARAM_INT);
                $stmt->bindValue(':dt', $prediction->dt, PDO::PARAM_INT);
                $stmt->bindValue(':temp', $prediction->temp, PDO::PARAM_STR);
                $stmt->bindValue(':rain', $prediction->rain, PDO::PARAM_STR);
                $stmt->bindValue(':windSpeed', $prediction->windSpeed, PDO::PARAM_STR);
                $stmt->bindValue(':windDegrees', $prediction->windDegrees, PDO::PARAM_STR);
                $stmt->execute();
                $prediction->predictionID = $file_db->lastInsertId();
            }
        }
        catch(PDOException $e){

            echo $e;
        }
        try {
            $updateQuery = "UPDATE Places SET lastUpdateDT=:lastUpdateDT WHERE placeID=:placeID";
            $stmt = $file_db->prepare($updateQuery);
            $stmt->bindValue(':placeID', $this->place->placeID, PDO::PARAM_INT);
            $stmt->bindValue(':lastUpdateDT', time(), PDO::PARAM_INT);
            $stmt->execute();

        }
        catch(PDOException $e) {

            echo $e;
        }

         $file_db = null;

    }
    function getPredictionsFromDB(){
        //placeID, dt, temp, rain, windSpeed, windDegrees
        $file_db = $this->getDB();
        try {
            $selectQuery = "SELECT placeID, dt, temp, rain, windSpeed, windDegrees FROM Predictions WHERE placeID = :placeID";
            $stmt = $file_db->prepare($selectQuery);
            $stmt->bindValue(':placeID', $this->place->placeID, PDO::PARAM_INT);
            $stmt->execute();
            $queryResponse = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $file_db= null;
            foreach ($queryResponse as $prediction){

                $newPrediction = new Prediction();
                $newPrediction->placeID = $prediction['placeID'];
                $newPrediction->dt = $prediction['dt'];
                $newPrediction->temp = $prediction['temp'];
                $newPrediction->rain = $prediction['rain'];
                $newPrediction->windSpeed = $prediction['windSpeed'];
                $newPrediction->windDegrees = $prediction['windDegrees'];

                array_push($this->listOfPredictions, $newPrediction);
            }

        }
        catch(PDOException $e){

            echo $e;
        }
    }
    function getDailyPredictions(){

        date_default_timezone_set('Africa/Bamako');
        $currentDT = strtotime('today midnight');

        for ($day = 1; $day < 6; $day++){

            $verbalPrediction = new VerbalPrediction();
            $verbalPrediction->dt = $currentDT;
            foreach ($this->listOfPredictions as $prediction) {


                if (($prediction->dt >= ($currentDT)) & ($prediction->dt < ($currentDT+86400))){

                    if ($prediction->dt == $currentDT) {

                        //00:00 (night 1)
                        $verbalPrediction->night_1_rain = $prediction->rain;
                        $verbalPrediction->night_1_temp = $prediction->temp;
                        $verbalPrediction->night_1_windDirection = $prediction->windDegrees;
                        $verbalPrediction->night_1_windSpeed = $prediction->windSpeed;
                        //echo json_encode($verbalPrediction);
                    }

                    elseif ($prediction->dt == ($currentDT + 10800) ) {

                        //03:00 (night 2)
                        //echo "Night 2 - ". $day.'</br/';

                        $verbalPrediction->night_2_rain = $prediction->rain;
                        $verbalPrediction->night_2_temp = $prediction->temp;
                        $verbalPrediction->night_2_windDirection = $prediction->windDegrees;
                        $verbalPrediction->night_2_windSpeed = $prediction->windSpeed;
                    }
                    elseif ($prediction->dt == ($currentDT + 21600 )) {

                        //06:00 (morning 1)
                        //echo "Morning 1 - ". $day.'</br/';
                        $verbalPrediction->morning_1_rain = $prediction->rain;
                        $verbalPrediction->morning_1_temp = $prediction->temp;
                        $verbalPrediction->morning_1_windDirection = $prediction->windDegrees;
                        $verbalPrediction->morning_1_windSpeed = $prediction->windSpeed;
                    }
                    elseif ($prediction->dt == ($currentDT + 32400)) {

                        //09:00 (morning 2)
                        //echo "Morning 2 - ". $day.'</br/';
                        $verbalPrediction->morning_2_rain = $prediction->rain;
                        $verbalPrediction->morning_2_temp = $prediction->temp;
                        $verbalPrediction->morning_2_windDirection = $prediction->windDegrees;
                        $verbalPrediction->morning_2_windSpeed = $prediction->windSpeed;
                    }
                    elseif ($prediction->dt == ($currentDT + 43200)) {

                        //12:00 (afternoon 1)
                        //echo "Afternoon 1 - ". $day.'</br/';
                        $verbalPrediction->afternoon_1_rain = $prediction->rain;
                        $verbalPrediction->afternoon_1_temp = $prediction->temp;
                        $verbalPrediction->afternoon_1_windDirection = $prediction->windDegrees;
                        $verbalPrediction->afternoon_1_windSpeed = $prediction->windSpeed;
                    }
                    elseif ($prediction->dt == ($currentDT + 54000)) {

                        //15:00 (afternoon 2)
                        //echo "Afternoon 2 - ". $day.'</br/';
                        $verbalPrediction->afternoon_2_rain = $prediction->rain;
                        $verbalPrediction->afternoon_2_temp = $prediction->temp;
                        $verbalPrediction->afternoon_2_windDirection = $prediction->windDegrees;
                        $verbalPrediction->afternoon_2_windSpeed = $prediction->windSpeed;
                    }
                    elseif ($prediction->dt == ($currentDT + 64800)) {

                        //18:00 (evening 1)
                        //echo "Evening 1 - ". $day.'</br/';
                        $verbalPrediction->evening_1_rain = $prediction->rain;
                        $verbalPrediction->evening_1_temp = $prediction->temp;
                        $verbalPrediction->evening_1_windDirection = $prediction->windDegrees;
                        $verbalPrediction->evening_1_windSpeed = $prediction->windSpeed;

                    }

                    else {

                        //21:00 (evening 2)
                        //echo "Evening 2 - ". $day.'</br/';
                        $verbalPrediction->evening_2_rain = $prediction->rain;
                        $verbalPrediction->evening_2_temp = $prediction->temp;
                        $verbalPrediction->evening_2_windDirection = $prediction->windDegrees;
                        $verbalPrediction->evening_2_windSpeed = $prediction->windSpeed;
                        array_push($this->listOfVerbalPredictions, $verbalPrediction);
                    }
                }
            }

            $currentDT = ($currentDT + 86400);
        }
        //echo json_encode($this->listOfVerbalPredictions);

    }
    function readConfigFile(){

        $configuration = json_decode(file_get_contents("config.json"));
        $this->isOffline = $configuration->work_offline;
        $this->APIkey = $configuration->APIkey;
        $this->systemCountry = $configuration->systemCountry;

    }
    function saveCallToDB(){

        $file_db = $this->getDB();
        $insert = "INSERT INTO CallLog (countryID, languageID, regionID, placeID, days, timeStamp) VALUES (:countryID, :languageID, :regionID, :placeID, :days, :timeStamp )";
        $stmt = $file_db->prepare($insert);

        $date = date_create();
        date_timestamp_get($date);
        $stmt->bindParam(':countryID', $_GET['country']);
        $stmt->bindParam(':languageID', $_GET['language']);
        $stmt->bindParam(':regionID', $_GET['region']);
        $stmt->bindParam(':placeID', $_GET['place']);
        $stmt->bindParam(':days', $_GET['days']);
        $stmt->bindParam(':timeStamp', date_timestamp_get($date));

        try {
            $stmt->execute();
            $file_db = null;
        }

        catch (PDOException $e) {

        }

    }


    function queryControl(){

	
	
        if (isset($_GET['days']) & isset($_GET['place']) & isset($_GET['region']) & isset($_GET['country'])& isset($_GET['language'])) {

            $this->country = $this->getCountryByIdFromDB($_GET['country']);
            $this->region = $this->getRegionByIdFromDB($_GET['region']);
            $this->place = $this->getPlaceByPlaceID($_GET['place']);
            $this->language = $this->getLanguageByLanguageID($_GET['language']);
            $this->days = $_GET['days'];
            $this->saveCallToDB();
            
	    
            
	    
            if ($this->updateNeeded()){

                if($this->isOffline == "true"){

                    
                    $this->getOfflineWeatherData();
                }
                else {

                    
                    $this->getOnlineWeatherData();

                }
                //$this->getOnlineWeatherData();
                $this->insertPredictionsIntoDB();
            }
            else {

                //echo "Getting from DB";
                $this->getPredictionsFromDB();
            }

            $this->getDailyPredictions();
            $this->vxmlPredictions();


        }
        elseif (isset($_GET['place']) & isset($_GET['region']) & isset($_GET['country'])& isset($_GET['language'])){

            $this->country = $this->getCountryByIdFromDB($_GET['country']);
            $this->region = $this->getRegionByIdFromDB($_GET['region']);
            $this->place = $this->getPlaceByPlaceID($_GET['place']);
            $this->language = $this->getLanguageByLanguageID($_GET['language']);
            $this->vxmlSelectDays();
            $this->flushXML();

        }
        elseif (isset($_GET['region']) & isset($_GET['country'])& isset($_GET['language'])){


            $this->country = $this->getCountryByIdFromDB($_GET['country']);
            $this->region = $this->getRegionByIdFromDB($_GET['region']);
            $this->language = $this->getLanguageByLanguageID($_GET['language']);
            $this->vxmlSelectPlace();
            $this->flushXML();


        }
        elseif (isset($_GET['country']) & isset($_GET['language'])){

            /*
             * forecast.php?country=1
             * The welcome message needs to be given first and then a region needs to be chosen
             */

            $this->country = $this->getCountryByIdFromDB($_GET['country']);
            $this->language = $this->getLanguageByLanguageID($_GET['language']);

            $this->vxmlWelcome();
            $this->flushXML();

        }
        elseif (isset($_GET['country'])){

            // choose a language
            $this->country = $this->getCountryByIdFromDB($this->systemCountry);
            $this->getLanguages($this->country->countryID);
            $this->vxmlSelectLanguage();
            $this->flushXML();

        }

    }

}

/*
 * This is where the application starts.
 */
$nMain = new Main();

$nMain->queryControl();








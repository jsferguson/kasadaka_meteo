/**
 * Created by jari on 15/05/16.
 */
$(function() {
    $( "#tabs" ).tabs();
    initialise();

    /*
     *  Assign html elements to variables: languages tab
     */
    var languagesTabel          = $('#languagesTabel');
    var frmAddLanguage          = $('#frmAddLanguage');
    var txtNewLanguageName      = $('#txtName');
    var txtNewLanguageCode      = $('#txtCode');

    /*
     *  Assign html elements to variables: countries tab
     */
    var countriesTable              = $('#countriesTable');
    var supportedLanguagesTable     = $('#supportedLanguagesTable');
    var txtCountryName              = $('#txtCountryName');
    var txtCountryCode              = $('#txtCountryCode');
    var txtTimeZone                 = $('#txtTimeZone');
    var frmAddCountry               = $('#frmAddCountry');
    var DDLlanguageSelect           = $('#DDLlanguageSelect');
    var btnAddSupportedLanguage     = $('#btnAddSupportedLanguage');
    var manageSupportedLanguages    = $('#manageSupportedLanguages');

    /*
     *  Assign html elements to variables: regions tab
     */
    var DDLCountrySelect            = $('#DDLCountrySelect');
    var RegionTable                 = $('#RegionTable');
    var frmAddRegion                = $('#frmAddRegion');
    var txtRegionName               = $('#txtRegionName');

    /*
     *  Assign html elements to variables: places tab
     */
    var DDLCountrySelect4Places     = $('#DDLCountrySelect4Places');
    var DDLRegionSelect4Places      = $('#DDLRegionSelect4Places');
    var frmAddPlace                 = $('#frmAddPlace');
    var txtPlaceName                = $('#txtPlaceName');
    var txtLattitude                = $('#txtLattitude');
    var txtLongtitude               = $('#txtLongtitude');
    var PlacesTable                 = $('#PlacesTable');

    /*
     *  Assign html elements to variables: audio clips tab
     */
    var DDLLanguageSelect4Audio     = $('#DDLLanguageSelect4Audio');
    var btnUploadAudio              = $('#btnUploadAudio');
    var audio_file_input            = $('#audio_file_input');
    var missingLanguagesTable       = $('#missingLanguagesTable');

    /*
     *  Assign html elements to variables: System
     */
    var btnDownloadCallLog          = $('#btnDownloadCallLog');
    var txtAPIkey                   = $('#txtAPIkey');
    var DDLCountrySelectForSystem   = $('#DDLCountrySelectForSystem');
    var DDLOnLineOffLineMode        = $('#DDLOnLineOffLineMode');
    var frmSystemConfig             = $('#frmSystemConfig');


    // Remember for which country the user may add a supported language
    var selectedCountryForLanguages = null;
    var oldLanguageCode = null;
    var selectedLanguageCode = null;

    function initialise(){

        dbGetLanguages();
        dbGetCountries();

    }

    /*
     *  Functions regarding the language tab
     */
    function dbGetLanguages(){

        $.getJSON("./php/get_languages.php", function(data){

            refreshLanguagesTabel(data);
            refreshLanguages4AudioClipsDDL(data);
            manageSupportedLanguages.hide();
        });
    }
    function dbInsertLanguage(){

            var formdata = {

                'name'           : txtNewLanguageName.val(),
                'code'           : txtNewLanguageCode.val().replace(" ", "")

            };


            $.ajax({
                    type         : 'POST',
                    url          : './php/submit_new_language.php',
                    data         : formdata,
                    dataType     : 'json',
                    encode       : true
                })
                .complete(function(){

                    txtNewLanguageName.val('');
                    txtNewLanguageCode.val('');
                    dbGetLanguages();
                });

        }
    function dbUpdateLanguage(updateData) {

        $.ajax({
                type         : 'POST',
                url          : './php/update_language.php',
                data         : updateData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                dbGetLanguages();
            });
    }
    function dbDeleteLanguage(deleteData) {

        $.ajax({
                type         : 'POST',
                url          : './php/delete_language.php',
                data         : deleteData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                dbGetLanguages();
            });
    }
    function refreshLanguagesTabel(languageData){


        data = '<table id = "languagesTabel"><tr><td>Name</td><td>Code</td><td></td><td></td></tr>';

        for (var i = 0; i < languageData.length; i++ ) {

            data = data + '<tr id = ' + languageData[i].languageID + '><td><input id="txtName" type="text" name="txtName" size="10" value="'+ languageData[i].name  +'"></td><td><input id="txtCode" type="text" name="txtCode" size="5" value="' + languageData[i].code+ '"></td><td><button class="btnUpdateLanguage">Update</button></td><td><button class="btnDeleteLanguage">Delete</button></td></tr>';

        }

        data = data + '</table>';
        languagesTabel.html(data);
    }

    /*
     *  When a user wants to update a language
     */
    languagesTabel.on('click', '.btnUpdateLanguage', function(){

        var languageID = $(this).closest('tr').attr('id');
        var language = $(this).closest("tr").find("#txtName").val();
        var code = $(this).closest("tr").find("#txtCode").val().replace(" ", "");
        var updateData = {

            'name'          : language,
            'code'          : code,
            'languageID'    : languageID,
            'old_code'      : oldLanguageCode

        };
        dbUpdateLanguage(updateData);

    });
    languagesTabel.on('click', '#txtCode', function(){

        oldLanguageCode = $(this).closest("tr").find("#txtCode").val();
    });

    /*
     *  When a user wants to delete a language
     */
    languagesTabel.on('click', '.btnDeleteLanguage', function(){

        var languageID = $(this).closest('tr').attr('id');
        var language = $(this).closest("tr").find("#txtName").val();
        var code = $(this).closest("tr").find("#txtCode").val();
        var deleteData = {

            'languageID'    : languageID,
            'code'          : code
        };

        dbDeleteLanguage(deleteData);
        dbUpdateLanguage();
    });

    /*
     *  When a user submits a new language
     */
    frmAddLanguage.submit(function(event){

        dbInsertLanguage();
        event.preventDefault();
    });


    /*
     *  Functions regarding the countries tab
     */
    function dbGetCountries(){

        $.getJSON("./php/get_countries.php", function(data){

            refreshCountriesTable(data);
            DDLCountrySelect.empty();
            refreshCountriesDDLForRegionManager(data);
            DDLCountrySelect4Places.empty();
            refreshCountries4PlacesDDL(data);
            refreshCountries4SystemDDL(data);
            getSettingsFromConfigFile();

        });
    }
    function dbInsertCountry(){

        var formData = {

            'name'      : txtCountryName.val().replace(" ", ""),
            'code'      : txtCountryCode.val().replace(" ", ""),
            'timezone'  : txtTimeZone.val().replace(" ", "")
        };

        $.ajax({
                type         : 'POST',
                url          : './php/submit_new_country.php',
                data         : formData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                txtCountryName.val('');
                txtCountryCode.val('');
                txtTimeZone.val('');
                dbGetCountries();
            });

    }
    function dbInsertSupportedLanguage(insertData){

        $.ajax({
                type         : 'POST',
                url          : './php/submit_supported_language.php',
                data         : insertData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                dbGetSupportedLanguages(insertData);
                dbGetUnsupportedLanguages(insertData);

            });

    }
    function dbGetSupportedLanguages(countryIdData){

        $.ajax({
            type: 'POST',
            url: './php/get_supported_languages_by_country_id.php',
            data: countryIdData,
            dataType: 'json',
            encode: true,
            complete: function (data) {

                refreshSupportedLanguagesTable(data.responseJSON);
            }
        });

    }
    function dbGetUnsupportedLanguages(countryIdData){

        $.ajax({
            type: 'POST',
            url: './php/get_unsupported_languages_by_country_id.php',
            data: countryIdData,
            dataType: 'json',
            encode: true,
            complete: function (data) {

                fillDDLUnusedLanguages(data.responseJSON);
            }
        });

    }
    function dbUpdateCountry(updateData) {

        $.ajax({
                type         : 'POST',
                url          : './php/update_country.php',
                data         : updateData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                dbGetCountries();
            });
    }
    function dbDeleteCountry(deleteData) {

        $.ajax({
                type         : 'POST',
                url          : './php/delete_country.php',
                data         : deleteData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                dbGetCountries();
            });
    }
    function dbDeleteSupportedLanguage(deleteData) {

        $.ajax({
                type         : 'POST',
                url          : './php/delete_supported_language.php',
                data         : deleteData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                dbGetSupportedLanguages(deleteData);
                dbGetUnsupportedLanguages(deleteData);
            });
    }
    function refreshCountriesTable(countriesData){

        data = '<table id = "countriesTable"><tr><td>Name</td><td>Code</td><td>Timezone</td><td></td><td></td><td></td></tr>';

        for (var i = 0; i < countriesData.length; i++ ) {

            data = data + '<tr id = ' + countriesData[i].countryID + '><td><input id="txtCountryName" type="text" name="txtCountryName" size="10" value="'+ countriesData[i].name  +'"></td><td><input id="txtCountryCode" type="text" name="txtCountryCode" size="5" value="' + countriesData[i].code+ '"></td><td><input id="txtTimeZone" type="text" name="txtTimeZone" size="15" value="' + countriesData[i].timezone+ '"></td><td><button class="btnUpdateCountry">Update</button></td><td><button class="btnDeleteCountry">Delete</button></td><td><button class="btnViewLanguages">View Languages</button></td></tr>';

        }

        data = data + '</table>';
        countriesTable.html(data);
    }
    function fillDDLUnusedLanguages(languagesData){

        DDLlanguageSelect.empty();

        if (languagesData[0].languageID != null){
            DDLlanguageSelect.append("<option value = '' selected disabled>Choose a language</option>");
            for (var i=0; i<languagesData.length; i++){

                DDLlanguageSelect.append('<option value=' + languagesData[i].languageID + '>' + languagesData[i].name + '</option>');
            }
        }
        else {

            DDLlanguageSelect.append("<option value = '' selected disabled>No more languages available</option>");
        }

    }
    countriesTable.on('click', '.btnDeleteCountry', function(){

        var countryID = $(this).closest('tr').attr('id');

        var deleteData = {

            'countryID'     : countryID

        };
        dbDeleteCountry(deleteData);

    });
    countriesTable.on('click', '.btnViewLanguages', function(){

        var countryID = $(this).closest('tr').attr('id');
        var countryName = $(this).closest('tr').find("#txtCountryName").val();
        $('#supported_languages').text("Supported languages for: " + countryName);
        var countryData = {

            'countryID'     : countryID
        };
        selectedCountryForLanguages = countryID;
        dbGetSupportedLanguages(countryData);
        dbGetUnsupportedLanguages(countryData);
        manageSupportedLanguages.show();

    });
    countriesTable.on('click', '.btnUpdateCountry', function(){

        var countryID = $(this).closest('tr').attr('id');
        var countryName = $(this).closest('tr').find("#txtCountryName").val().replace(" ", "");
        var countryCode = $(this).closest("tr").find("#txtCountryCode").val().replace(" ", "");
        var countryTimezone = $(this).closest("tr").find("#txtTimeZone").val().replace(" ", "");
        var updateData = {

            'name'          : countryName,
            'code'          : countryCode,
            'timezone'      : countryTimezone,
            'countryID'     : countryID

        };
        dbUpdateCountry(updateData);

    });
    btnAddSupportedLanguage.click(function(){


        var languageID = DDLlanguageSelect.val();
        var countryID = selectedCountryForLanguages;
        var insertData = {
            "countryID":    countryID,
            "languageID":   languageID
        };
        dbInsertSupportedLanguage(insertData)
    });
    supportedLanguagesTable.on('click', '.btnDeleteSupportedLanguage', function(){


        var languageID = $(this).closest('tr').attr('id');
        var countryID = selectedCountryForLanguages;
        var deleteData = {

            "languageID"    : languageID,
            "countryID"     : countryID
        };
        dbDeleteSupportedLanguage(deleteData);


    });
    function refreshSupportedLanguagesTable(languagesData){

        data = '<table id = "supportedLanguagesTable"><tr><td>Name</td><td></td></tr>';

        for (var i = 0; i < languagesData.length; i++ ) {

            data = data + '<tr id = ' + languagesData[i].languageID + '><td>'+ languagesData[i].name  +'</td><td><button class="btnDeleteSupportedLanguage">Delete</button></td></tr>';

        }

        data = data + '</table>';
        supportedLanguagesTable.html(data);
    }
    frmAddCountry.submit(function(event){

        event.preventDefault();
        dbInsertCountry();
    });

    /*
     *  Functions regarding regions tab
     */
    function dbGetRegionsByCountryID(countryIdData){

        $.ajax({
            type: 'POST',
            url: './php/get_regions_by_country_id.php',
            data: countryIdData,
            dataType: 'json',
            encode: true,
            complete: function (data) {

                refreshRegionsTable(data.responseJSON);
            }
        });

    }
    function dbInsertNewRegion(){

        var insertData = {

            "countryID" : DDLCountrySelect.val(),
            "name"      : txtRegionName.val().replace(" ", "")
        };
        $.ajax({
                type         : 'POST',
                url          : './php/submit_new_region.php',
                data         : insertData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                var countryIdData = {

                    "countryID" : DDLCountrySelect.val()
                };
                txtRegionName.val('');
                dbGetRegionsByCountryID(countryIdData);
            });

    }
    function dbUpdateRegion(updateData) {

        var countryID = {
            "countryID" : DDLCountrySelect.val()
        };

        $.ajax({
                type         : 'POST',
                url          : './php/update_region.php',
                data         : updateData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                dbGetRegionsByCountryID(countryID);
            });
    }
    function dbDeleteRegion(deleteData) {

        var countryID = {
            "countryID" : DDLCountrySelect.val()
        };
        $.ajax({
                type         : 'POST',
                url          : './php/delete_region.php',
                data         : deleteData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                dbGetRegionsByCountryID(countryID);
            });
    }
    function refreshCountriesDDLForRegionManager(countriesData){

        DDLCountrySelect.append("<option value = '' selected disabled>Choose a country</option>");
        for (var i=0; i<countriesData.length; i++){

            DDLCountrySelect.append('<option value=' + countriesData[i].countryID + '>' + countriesData[i].name + '</option>');
        }
    }
    function refreshRegionsTable(regionsData){

        RegionTable.show();
        data = '<table id = "regionTable"><tr><td>Region name</td><td></td><td></td></tr>';

        for (var i = 0; i < regionsData.length; i++ ) {

            data = data + '<tr id = ' + regionsData[i].regionID + '><td><input id="txtRegionName" type="text" name="txtRegionName" value="'+ regionsData[i].name  +'"></td><td><button class="btnUpdateRegion">Update</button></td><td><button class="btnDeleteRegion">Delete</button></td></tr>';

        }

        data = data + '</table>';
        RegionTable.html(data);
    }

    DDLCountrySelect.change(function(){

        var countryIdData = {
            "countryID" : DDLCountrySelect.val()
        };

        dbGetRegionsByCountryID(countryIdData);
    });
    frmAddRegion.submit(function(event){

        dbInsertNewRegion();
        event.preventDefault();
    });
    RegionTable.on('click','.btnUpdateRegion', function(){


        var regionID = $(this).closest('tr').attr('id');
        var regionName = $(this).closest('tr').find("#txtRegionName").val().replace(" ", "");
        var countryID = DDLCountrySelect.val();

        var updateData = {

            'name'          : regionName,
            'regionID'      : regionID,
            'countryID'     : countryID

        };
        dbUpdateRegion(updateData);
    });
    RegionTable.on('click','.btnDeleteRegion', function(){


        var regionID = $(this).closest('tr').attr('id');
        var regionName = $(this).closest('tr').find("#txtRegionName").val();
        var countryID = DDLCountrySelect.val();

        var deleteData = {


            'regionID'      : regionID


        };
        dbDeleteRegion(deleteData);
    });

    /*
     *  Functions regarding places tab
     */
    function dbGetRegions4PlacesByCountryID(countryIdData){

        $.ajax({
            type: 'POST',
            url: './php/get_regions_by_country_id.php',
            data: countryIdData,
            dataType: 'json',
            encode: true,
            complete: function (data) {

                refreshRegions4PlacesDDL(data.responseJSON);
            }
        });

    }
    function dbGetPlacesByRegionID(regionData){

        $.ajax({
            type: 'POST',
            url: './php/get_places_by_region_id.php',
            data: regionData,
            dataType: 'json',
            encode: true,
            complete: function (data) {

                refreshPlacesTable(data.responseJSON);
            }
        });

    }
    function dbUpdatePlace(updateData) {

        $.ajax({
                type         : 'POST',
                url          : './php/update_place.php',
                data         : updateData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                var regionData = {
                    "regionID" : DDLRegionSelect4Places.val()
                };
                dbGetPlacesByRegionID(regionData);
            });
    }
    function dbDeletePlace(deleteData) {

        $.ajax({
                type         : 'POST',
                url          : './php/delete_place.php',
                data         : deleteData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                var regionData = {
                    "regionID" : DDLRegionSelect4Places.val()
                };
                dbGetPlacesByRegionID(regionData);
            });
    }
    function dbInsertPlace(){

        var insertData = {

            "regionID"      : DDLRegionSelect4Places.val(),
            "name"          : txtPlaceName.val().replace(" ", ""),
            "lattitude"     : txtLattitude.val(),
            "longtitude"    : txtLongtitude.val()
        };
        $.ajax({
                type         : 'POST',
                url          : './php/submit_new_place.php',
                data         : insertData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(){

                var regionData = {
                    "regionID" : DDLRegionSelect4Places.val()
                };
                txtPlaceName.val('');
                txtLattitude.val('');
                txtLongtitude.val('');
                dbGetPlacesByRegionID(regionData);


            });

    }
    function refreshCountries4PlacesDDL(countriesData){

        DDLCountrySelect4Places.empty();
        DDLCountrySelect4Places.append("<option value = '' selected disabled>Choose a country</option>");
        for (var i=0; i<countriesData.length; i++){

            DDLCountrySelect4Places.append('<option value=' + countriesData[i].countryID + '>' + countriesData[i].name + '</option>');
        }

    }
    function refreshRegions4PlacesDDL(regionsData){

        DDLRegionSelect4Places.empty();
        DDLRegionSelect4Places.append("<option value = '' selected disabled>Choose a region</option>");
        for (var i=0; i<regionsData.length; i++){

            DDLRegionSelect4Places.append('<option value=' + regionsData[i].regionID + '>' + regionsData[i].name + '</option>');
        }

    }
    function refreshPlacesTable(placesData){

        PlacesTable.show();
        data = '<table id = "placesTable"><tr><td>Name</td><td>Latitude</td><td>Longtitude</td><td></td><td></td></tr>';

        for (var i = 0; i < placesData.length; i++ ) {

            data = data + '<tr id = ' + placesData[i].placeID + '><td><input id="txtPlaceName" type="text" name="txtPlaceName" value="'+ placesData[i].name  +'"></td><td><input id="txtLattitude" type="text" name="txtLattitude" size="10" value="'+ placesData[i].lattitude  +'"></td><td><input id="txtLongtitude" type="text" name="txtLongtitude" size="10"value="'+ placesData[i].longtitude  +'"></td><td><button class="btnUpdatePlace">Update</button></td><td><button class="btnDeletePlace">Delete</button></td></tr>';

        }

        data = data + '</table>';
        PlacesTable.html(data);
    }

    DDLCountrySelect4Places.change(function(){

        PlacesTable.empty();
        var countryIdData = {
            "countryID" : DDLCountrySelect4Places.val()
        };

        dbGetRegions4PlacesByCountryID(countryIdData);
    });
    DDLRegionSelect4Places.change(function(){

        var regionData = {
            "regionID" : DDLRegionSelect4Places.val()
        };

        dbGetPlacesByRegionID(regionData);
    });
    PlacesTable.on('click','.btnUpdatePlace', function(){


        var placeID = $(this).closest('tr').attr('id');
        var placeName = $(this).closest('tr').find("#txtPlaceName").val().replace(" ", "");
        var lattitude = $(this).closest('tr').find("#txtLattitude").val();
        var longtitude = $(this).closest('tr').find("#txtLongtitude").val();
        var regionID = DDLRegionSelect4Places.val();


        var updateData = {

            'name'          : placeName,
            'placeID'       : placeID,
            'regionID'      : regionID,
            'lattitude'     : lattitude,
            'longtitude'    : longtitude


        };
        dbUpdatePlace(updateData);
    });
    PlacesTable.on('click','.btnDeletePlace', function(){


        var placeID = $(this).closest('tr').attr('id');
        var deleteData = {


            'placeID'       : placeID

        };
        dbDeletePlace(deleteData);
    });
    frmAddPlace.submit(function(event){

        event.preventDefault();
        if ($.isNumeric(txtLattitude.val()) && $.isNumeric(txtLongtitude.val()) ){

            dbInsertPlace();
        }
        else {

            alert("The coordinates should be numerical!");
        }

    });

    /*
     *  Functions regarding audio clips tab
     */
    function dbGetMissingAudioFiles(){

        var formData = {
            "languageCode" : DDLLanguageSelect4Audio.val()
        };
        $.ajax({
            type: 'POST',
            url: './php/get_missing_language_files.php',
            data: formData,
            dataType: 'json',
            encode: true,
            complete: function (data) {

                refreshAudioClipsTable(data.responseJSON);
            }
        });
    }
    function refreshLanguages4AudioClipsDDL(languagesData){

        DDLLanguageSelect4Audio.empty();
        DDLLanguageSelect4Audio.append("<option value = '' selected disabled>Choose a languages</option>");
        for (var i=0; i<languagesData.length; i++){

            DDLLanguageSelect4Audio.append('<option value=' + languagesData[i].code + '>' + languagesData[i].name + '</option>');
        }

    }
    function refreshAudioClipsTable(audioFileData){

        missingLanguagesTable.show();
        data = '<table id = "missingAudioFilesTable"><tr><td><strong>Missing Files (these are needed for the system to work correctly):</strong></td></tr>';

        for (var i = 0; i < audioFileData.length; i++ ) {

            data = data + '<tr><td>'+ audioFileData[i].name  +'.wav</td></tr>';

        }

        data = data + '</table>';
        missingLanguagesTable.html(data);
    }
    DDLLanguageSelect4Audio.change(function(){

        var languageData = {
            "languageID" : DDLLanguageSelect4Audio.val()
        };

        selectedLanguageCode = DDLLanguageSelect4Audio.val();
        audio_file_input.prop('disabled', false);

        dbGetMissingAudioFiles();
    });

    audio_file_input.change(function(){

        btnUploadAudio.prop('disabled', false);
    });
    btnUploadAudio.on('click', function(){


        var formData = new FormData();
        var files = $('#audio_file_input')[0];
        for (var i = 0, len = files.files.length; i < len; i++) {
            //formData.append("file" + i, files.files[i]);
            //formData.append("my_file", files);
            formData.append("my_file[]", files.files[i]);
        }
        formData.append("code", selectedLanguageCode);



        console.log("sending files");
        //send formdata to server-side
        $.ajax({
            url: "./php/process_files.php", // our php file
            type: 'post',
            data: formData,
            dataType: 'html', // we return html from our php file
            async: true,
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success : function(data) {

                dbGetMissingAudioFiles();
                console.log("OK");
                alert("Files uploaded and converted !")
            },
            error : function(request) {
                
                console.log(request);
            }
        });


    });

    /*
     *  Functions regarding the systems tab
     */
    btnDownloadCallLog.on('click', function(){

        window.open('./php/get_call_log_csv.php');
    });
    function getSettingsFromConfigFile(){



        $.getJSON("././config.json", function(data){

            refreshSystemSettings(data);
        });
    }
    function refreshCountries4SystemDDL(countriesData){

        DDLCountrySelectForSystem.empty();

        for (var i=0; i<countriesData.length; i++){

            DDLCountrySelectForSystem.append('<option value=' + countriesData[i].countryID + '>' + countriesData[i].name + '</option>');
        }

    }

    function refreshSystemSettings(data){

        DDLCountrySelectForSystem.val(data.systemCountry);
        txtAPIkey.val(data.APIkey);
        if (data.work_offline == "true"){
            DDLOnLineOffLineMode.val(1);
        }
        else {

            DDLOnLineOffLineMode.val(0);
        }
    }
    frmSystemConfig.submit(function(event){

        event.preventDefault();

        var workmode=false;

        if (DDLOnLineOffLineMode.val() == 1){
            workmode = true;
        }
        var insertData = {

            "work_offline"      : workmode,
            "APIkey"            : txtAPIkey.val(),
            "systemCountry"     : DDLCountrySelectForSystem.val()
        };
        $.ajax({
                type         : 'POST',
                url          : './php/save_configuration.php',
                data         : insertData,
                dataType     : 'json',
                encode       : true
            })
            .complete(function(data){

            });
    });

});